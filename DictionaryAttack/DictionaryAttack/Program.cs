﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Collections;
using System.Text;
using System.Linq;

class Program
{
    static Dictionary<char, char> leet = new Dictionary<char, char>();
    static void Main(String[] args)
    {
        #region Dic
        var map = new Dictionary<string, string>();
        map["8esDbw8ZVmUuUMEy2Scf5qGiZYiykevrvKpq2bVYHj4="] = "a";
        map["3hoie8omUyvM/9Qfx9dKfoptlwemYe2os8aohTGzoyw="] = "i";
        map["Rz38Ng2qI3mPkaRB6uDoCYmmfzbVTCzpt2sG1o+TZqo="] = "o";
        map["N5aunKGHeN2WETLXLzfhfCxAfkwtGU/imziiTF3t7oY="] = "e";
        map["rjwtKqkPc7cfQ+zZ+E9c+fzQYhRvhVtaKEFb+srIHcQ="] = "u";
        map["tDdmKQpMiVDFA1YdblkHSFzL4Z9UIQ9FSouf3TybOu0="] = "password1";
        map["lUfxHX9xH2aOHheMMqQF+f5BNh97avew2uOwEN3B7HE="] = "0";
        map["h84yifAWGLj9sakEqxZ3QEjkXL42AoScP3L5Tdevm98="] = "1";
        map["r6BN0tdyAYZD0Nmc7bfV0WRcFBb1A2WIPPKHVRG59k8="] = "3";
        map["28AdfW0JHmCP4TbieGON8dafRaFpUgpzuX2bHZN6WsM="] = "4";
        map["/PtjJboZGlsmTovvyOhBOoTVnQKUP/gJXxjLAW9Lppw="] = "~";
        map["05HwH93tksb69U1ifesCQuYFP+gKPVH2L6W8JeBdXy0="] = "~";
        map["0BkyqI3NHyjh0m20wNt6txW08dglSMP4/qzUEezq4Aw="] = "~";
        map["1mT5cdKRz4BbfMdc8LAdnxfjsGO4lV0k0/V1IHtidmY="] = "~";
        map["28AdfW0JHmCP4TbieGON8dafRaFpUgpzuX2bHZN6WsM="] = "~";
        map["3hoie8omUyvM/9Qfx9dKfoptlwemYe2os8aohTGzoyw="] = "~";
        map["3uDaglIdYUn11AadEhELBjE15A0L6hAaWnZmjCGtt+M="] = "~";
        map["4D4NstIYSjVN826Q+SXUDDmXglJplpYWiJYf9rt7H8U="] = "~";
        map["7FCsEXCDTAxyLh3EPnNx7YrJ44SzehQYv3GmPSA6pWk="] = "~";
        map["81X3NN5JgTuGgqq3ErF0eL/l/wZFYkCwur6fZ06L2Us="] = "~";
        map["8FzGA/nS7XizLrAVOr/FoeKSq4gaoQRq+kpBKNXHIzc="] = "~";
        map["8OtpJ+E1XHv2RDsKIEwJc9KUFwPRzaqeHJ735Er6AVE="] = "~";
        map["8cdgZu9dBOrcTBMqElM+y9Vh5FTBRQ7n9EGa/4qVHUk="] = "~";
        map["8esDbw8ZVmUuUMEy2Scf5qGiZYiykevrvKpq2bVYHj4="] = "~";
        map["9DS4orbhPFbjJcosEqQg+eg0Si5qSOnftfHiqK8sYug="] = "~";
        map["9XDFIu4RPH0EL5XR+5VYILJ3UFAyltpfjONJKp/vcLk="] = "~";
        map["B2E/K/DywbLEKutOKpS8HxHFrZwucwac4KjzYgsXg3g="] = "~";
        map["BJQeqlV+4ejv0je5GpekzGdHHWHL5nnrbtD/170LZCA="] = "~";
        map["BjQiH/A2FUNHlUwhBi8NWmj3HmhmAh6Ag0kRyVSaVo8="] = "~";
        map["BwbAsOqPsxteVCpAwIjrhYogsUS1bF/bLns2QdcLYUI="] = "~";
        map["CYDZabjeyTAwcEDEcvrX83514UmpjzvQUQ68DIZ/PXg="] = "~";
        map["CornANxoZ5FJnlhwHmK42CDXf3h6jFr3g73YIRuoymQ="] = "~";
        map["DDa2TJX20pPsNftfyJ3s6LBwSMSR3EADZGDxW2wThbs="] = "~";
        map["FFXy3vru2D8rTWZRlh9lSMvtEusfWgO17OmJCTQTECs="] = "~";
        map["FGkqFC/jLDqDZ10fql1nGw7AQNWioOrZ3ydEaJyXBwo="] = "~";
        map["Fz+Y0H/R2rMZlc1C88Yx0A0xluYnVTinlw5qaSx8vWQ="] = "~";
        map["GcJMWMDF6+f+onf6oi1FbpnN7dVrFEZnlXtHqmaygs4="] = "~";
        map["GsXTQM0w+Clb1c9B7n28mADU2quLeI1n91KTyBboeHI="] = "~";
        map["HLnuqQmCYetzrau3frCDEpZ52QCIby108gugsmwAwQ0="] = "~";
        map["HWv9gx+GL/6g+0b0eOc+1Z/8BHse91/5T/DdiDwEknU="] = "~";
        map["IDB/pOthrWobzapJ/N8HsraNhwfbExAa2uusdiKHFFI="] = "~";
        map["IXYqlHbVeONERbxFe8SaEPEEKex45EihiC/l8CR52kk="] = "~";
        map["Idvs4Al9YZsqPG8xkSxVqb6MOVhbw5k+qtF8UZKYVE8="] = "~";
        map["IxQxcFXR51q8h8FLblPhYfUR30lIAt6hX8TjZWVa/GE="] = "~";
        map["JCmqBN0MsW13tEmsQPYWg9Fj25MUrqFYvSK2arxTt0A="] = "~";
        map["JOXxLH/i8i+fxDIWP2cts5Si/5En1A8M3s/vy6Aadic="] = "~";
        map["KudA8vCEQdGaaCSxotpAcluXnVPS3MAZPkwI/lVupak="] = "~";
        map["LGZEfbUr/tMREpJtsao/uuewcJXApmgfHDbh1zzfdhU="] = "~";
        map["LZIzmWEqXDPJsnKttFGRaG/jUhHrbTEKt1XCO6XbdME="] = "~";
        map["Lq0kV5M0HDSgB4m5KZbbn6BYRNlkKfaaAr3/11ueopY="] = "~";
        map["Lqxt1UjT1ecV6ucgYn+yrGSUTxPWkZgdDtbygGwC/BA="] = "~";
        map["MHQTB1MSsvhBxMpdRUiaM9Uj1QxU7zYq3FqDlW2HT2s="] = "~";
        map["MKewBZryb2l36Y0tDyx+WuVeXUGfiSzcJplm9y1w9m0="] = "~";
        map["N5aunKGHeN2WETLXLzfhfCxAfkwtGU/imziiTF3t7oY="] = "~";
        map["NmrOUzHxKSfNT8UJ1YXbRL8I+HCAb+glJ0bBXcHfagE="] = "~";
        map["NnSS+AuW1Z6zytSfqaiIVp4xxHHe70Av+IdhDlkoItQ="] = "~";
        map["O9L1ZWYwuzgaImTjOwuogXfpC+C44zzcDhpt08LjR5c="] = "~";
        map["R/ye+L9W+l6hyZ/v1POsWYboEGemIisARL8ohUvfBLI="] = "~";
        map["R1v9fEb9VrZuU5xiYTKTqhHF03VtXg7+KtfFHPkQuCQ="] = "~";
        map["RVfhsLovxa+/6tWgeSBASIIkzXkVtDPT4yYvjboHhIk="] = "~";
        map["Rz38Ng2qI3mPkaRB6uDoCYmmfzbVTCzpt2sG1o+TZqo="] = "~";
        map["S98FBzlv2vMVP/q+23m1wrHMJIrcy1rhoQGy338c4Bs="] = "~";
        map["SzraQWWasG5ZO1tJq16DqU/7M/o/WRiAWRl1aFFvwr8="] = "~";
        map["TInfNYwXvofBA+9QIe3+XEfDpO5ER+R+Jn3BOshhZWU="] = "~";
        map["VDkcRd54BhYlK5Wg2PPDa/jzGrSkMepGIv6Tw3I2ksc="] = "~";
        map["VjFTqTEY27V8lK2yCvhLhYm2Brh9bN1vWckVaevsiiY="] = "~";
        map["Wau4ReopjFKk8SYYNq5lIBL+Rgg8aBR6h9UgTIv2u7I="] = "~";
        map["XtEWsXf16y6Bc7vQxDy7hwRdBVWo3dV9C6CDVSf4PLs="] = "~";
        map["Y5b6UztMueFYIFMl4a61jlD/ZhFG74/rVn8XaqqU+8c="] = "~";
        map["YollqBlewcxE/kF6PKvv0r1CLZkKx82657bB0eQbiK0="] = "~";
        map["YzSqlQTtq5j+Kd+hW1ISgBW0mn61vsQsxNeipq0sYCo="] = "~";
        map["aJIH+q0YjYZCpierKtbue5JDtZSF8tKxVKuHYUPQ65k="] = "~";
        map["bi6rh2HgTbJxR2GOTNWZLlxiiWZVnObptGj0KqOCSYo="] = "~";
        map["cX7VyMvSYhuRvEfAUb3uNh8kmjpNFg0tatUPN562iOg="] = "~";
        map["eSCTiCrzHPbngPu3F4ivPkLUv7MqLUlmWAhA4UO975Y="] = "~";
        map["eUqkcVCbgIx1bGhhmnN5MxJFJhVruINmG65TjT/EQ1k="] = "~";
        map["gE7PseB3mspPtYG3JROzT9FeqTfPFYQvBF2SJD9V19Y="] = "~";
        map["gMi4hC4o7Fmv6yIrU48BVy8I1khXwkaD36G7bWiZHeo="] = "~";
        map["h84yifAWGLj9sakEqxZ3QEjkXL42AoScP3L5Tdevm98="] = "~";
        map["hEGKCiTZSA5x560hodRoIBBTE8pv4sP1VXG4D0fXWcI="] = "~";
        map["ix+0IJIpLpHeSHEII+Q/IVY+FlRXn3xMA0ey6UITi34="] = "~";
        map["j2GTUqtqZpotY8wF16zkvnbdCLTnX3oOZ30SjQUnIUk="] = "~";
        map["jtUg00EsmzzFkk8JgKg3OpkmPRpN9xbwsdNXQSPczwo="] = "~";
        map["k1J9Dv3EI442CO6A2FGN1H8JgFO2kjNBvkjDR0WIvkA="] = "~";
        map["kuRpkIh9kaNz6XvG8U6GO/IARH/SqhRnTiJbZHXC0yQ="] = "~";
        map["lFgqRrqTz1WXmO22u+Is1ZmWWUtuYrTJigsSB7I9NHI="] = "~";
        map["lUfxHX9xH2aOHheMMqQF+f5BNh97avew2uOwEN3B7HE="] = "~";
        map["m0IeuugWDOl9cFUFRYJhouCBT39T0dpp1xBOKPqHP94="] = "~";
        map["mgA5kgALstQpGUBp4vZ8oiz0P4jjAGl0wjgls6kQyMA="] = "~";
        map["nMAwaDYvEIAwoqtqWMpBAWdhuRgRq/fmwWbRM7cOMIU="] = "~";
        map["opBtoC66YDRbLNqZAAu2FIeKfF0HMOGHCOCPYeNHdx4="] = "~";
        map["ot/igM+me4e3UTT731qcBkSAcToyADMCddr7i7LCWGo="] = "~";
        map["qrkd/8imuRtiDb9N1w2hQRxJAkdx3Wqh1HVXPS7dym8="] = "~";
        map["r6BN0tdyAYZD0Nmc7bfV0WRcFBb1A2WIPPKHVRG59k8="] = "~";
        map["rdALvOYVhA3hnUTBlaQXigWBSgMYzGTreSKyMoAoKfw="] = "~";
        map["rjwtKqkPc7cfQ+zZ+E9c+fzQYhRvhVtaKEFb+srIHcQ="] = "~";
        map["rwvmTDiJxIEETbsngvpxYGwZZK+FGo7527odGuQUjtQ="] = "~";
        map["socJeO02bT2w+XZUrLoopbZvQ1lRhDfE88GVrJQ8p+g="] = "~";
        map["tDdmKQpMiVDFA1YdblkHSFzL4Z9UIQ9FSouf3TybOu0="] = "~";
        map["tojYiNtlWmq+7r1dSvxDk3W5at/NMAi1uxCHY61WAKk="] = "~";
        map["tqpGCBzhR+0ONFk1sBiHPhz+kRiXmY3CGdUXqnMJwLg="] = "~";
        map["uAZthS7b4ySZtWpM9pJ7ulYnhFdpFABpR2iPRQEmff0="] = "~";
        map["uCG9dSBejCOrZWsX7+u8G340p74s8lDS/El8MIeOyMo="] = "~";
        map["ugcIIpDID0R1uFqBAcN3PNXhwlhen77GdAccFgpbs+A="] = "~";
        map["usg8BTtSfewL5M3OVg91TJCTc5vONLqgUCC/Si1Grsg="] = "~";
        map["vs2sCU8qG0pDYQfcjlPzDzvcbJnhP1OgFRcXP4i3ffw="] = "~";
        map["wEtqAs8JHjicWnXshAWF5Sg6NoswuG9qeJ7USw7YD7c="] = "~";
        map["y+zbMpySKY+WF97KkgRQ+tBpM7iTqTj9guWmGJcqfyA="] = "~";
        map["y1R6JQiUzUovgtdrvCkbeQAyMhFoupzhI5ZuQVPfCgw="] = "~";
        map["zqKPAOt5ziHSeRxc0TgUZF3rJxzBHAKdJeccvt3F7Jg="] = "~";

        leet['a'] = '4';
        leet['e'] = '3';
        leet['i'] = '1';
        leet['o'] = '0';

        #endregion
        var start = DateTime.Now;
        Crack(map);
        var end = DateTime.Now;
        Console.WriteLine("Started: " + start.ToShortTimeString());
        Console.WriteLine("Ended: " + end.ToShortTimeString());
        Console.WriteLine("Total: " + (end - start).TotalMinutes + ":" + (end - start).TotalSeconds);
        Console.WriteLine("Found: " + map.Values.Count(x => x != "~"));

    }

    private static void Crack(Dictionary<string, string> map)
    {
        var salt = "IEEE";
        var pepper = "Xtreme";

        var words = File.ReadAllLines("WordList.txt");
        Console.WriteLine("Started");
        var counter = 0;
        foreach (var word in words)
        {
            var filtered = word.ToLower();/*.Replace("!","").Replace("@", "").Replace("#", "").Replace("$", "").Replace("%", "").Replace("^", "").Replace("&", "").Replace("*", "").Replace("(", "")
                .Replace(")", "").Replace("-", "").Replace("_", "").Replace("=", "").Replace("[", "").Replace("]", "").Replace("\\", "").Replace("/", "");*/
            foreach (string variation in GetVariations(filtered))
            {
                var hash = GetHashCode(salt + variation + pepper);
                if (map.ContainsKey(hash))
                {
                    CheckAndSavePass(map, variation, hash);
                }
            }
        }
    }

    private static HashSet<string> GetVariations(string word)
    {
        var list = new HashSet<string>();
        list.Add(word);
        
        for (int i = 0; i <= 9; i++)
        {
            var sb = new StringBuilder();
            sb.Append(word);
            sb.Append(i);
            list.Add(sb.ToString());
        }

        foreach(var x in leet)
        {
            if (word.Contains(x.Key + ""))
                list.UnionWith(GetVariations(word.Replace(x.Key, x.Value)));
        }
           
        return list;
    }

    private static void CheckAndSavePass(Dictionary<string, string> map, string pass, string hash)
    {
        Console.WriteLine(hash + " " + pass);
        map[hash] = pass;
        try
        {

            var contained = false;
            foreach (var x in File.ReadAllLines("found.txt"))
            {
                if (x.Contains(hash))
                {
                    contained = true;
                }
            }

            if (!contained)
            {
                File.AppendAllText("found.txt", "map[\"" + hash + "\"]=\"" + pass + "\";" + Environment.NewLine);
            }
        }
        catch (Exception e)
        {

        }
    }

    public static string GetHashCode(string p)
    {
        var a = new SHA256Managed();
        return Convert.ToBase64String(a.ComputeHash(new System.Text.UTF8Encoding().GetBytes(p)));
    }
}